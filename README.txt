INTRODUCTION
------------
XSS Protection

This is a simple and very useful module to protect your site from XSS attacks.

How it works?

Lot of XSS attacks happen due to script tags being put up in the URL and to execute them hackers use '<', '>' to enclose tags. We did some extensive testing on few of our production sites using some renowned XSS scanners and it was difficult to plug them all. This simple fix plugged all attacks and it has been months we have seen any XSS alert on any of them

This module is not a replacement for check_plain which is intended to be used wherever possible in open drupal forms, but a generic url filter to avoid hackers from exploiting URLs. So, we are trying to sanitize cross-site suspicious GET requests here.

It blocks vulnerability attacks by identify patterns in the URLs like '<', '>','%3E' and '%3C' and hence, prevent XSS Vulnerability injection.

REQUIREMENTS
------------


INSTALLATION
------------

  * Download this module to sites/all/modules/contrib

CONFIGURATION
-------------

  * Enable it: /admin/modules
  * Enable Xss Protection: /admin/config/system/xssprotection/settings
  * Optional: Write custom message to display to user on page

Make sure for all the default URLs in your Drupal site, you are replacing your are replacing '<', '>','%3E' and '%3C' with standard '-' using Pathauto module.

MAINTAINERS
-----------

Current maintainers:
 * Nilesh Chhatbar - https://www.drupal.org/user/2788025
