<?php

/**
 * @file
 * Module for managing XSS vulnerability.
 */

/**
 * Implements hook_form().
 */
function xssprotection_settings_form($form, &$form_state) {

  $form['xssprotection_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable the XSS protection'),
    '#default_value' => variable_get('xssprotection_enabled', 0),
  );
  $form['xssprotection_text_enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Enable text to be displayed after deflecting an attack.'),
    '#default_value' => variable_get('xssprotection_text_enabled', 0),
  );
  $form['xssprotection_text'] = array(
    '#title' => t('Message'),
    '#type' => 'textarea',
    '#description' => t('Text to display to user'),
    '#default_value' => variable_get('xssprotection_text', 'Requested page could not be found.'),
  );

  return system_settings_form($form);
}
